import API from "../DataApi";

export default {
  // Task API
  getProvince(data) {
    return API.get("province", data);
  },
};
