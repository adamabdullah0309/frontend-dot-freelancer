import API from "../DataApi";

export default {
  // Task API
  getCity(data) {
    return API.get(`city?province=${data.provinceId}`);
  },
};
