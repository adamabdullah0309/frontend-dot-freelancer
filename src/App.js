import React, { useEffect, useState } from "react";
import { notification, Layout, Radio, Drawer } from "antd";

import { CaretDownFilled } from "@ant-design/icons";

import { Switch, Route } from "react-router-dom";
import { useCookies } from "react-cookie";

import Sidebar from "./components/Sidebar";
import Navbar from "./components/Navbar";

import routes from "./helpers/route";
// import CompanyApi from "./services/company/Provider";
import "./App.css";

const { Footer } = Layout;
const Dates = new Date();

function App() {
  const [cookies, setCookie, removeCookie] = useCookies(["sessionUser"]);
  const [collapsed, setCollapsed] = useState(false);
  const [visibleModal, setVisibleModal] = useState(false);

  const openNotification = (placement, type, status) => {
    // console.log("Category Assets loaded", type);
    // console.log("Category Assets loaded", status);

    notification.open({
      message: "Logsitik Message",
      description: type,
      icon: (
        <CaretDownFilled
          type={status === "success" ? "check" : "close"}
          style={{ color: "#108ee9" }}
        />
      ),
      duration: 1.5,
      placement,
    });
  };

  const changeCollapsed = () => setCollapsed(!collapsed);

  const logout = async () => {
    await localStorage.removeItem("accessCode");
    await removeCookie("sessionUser");
  };

  const handleCancel = () => {
    setVisibleModal(true);
  };

  return (
    <Layout>
      <Sidebar collapsed={collapsed} />
      <Layout className="site-layout">
        <Navbar
          collapsed={collapsed}
          changeCollapsed={changeCollapsed}
          logout={logout}
          lastSession={localStorage.getItem("accessCode")}
          sessionUser={cookies.sessionUser}
        />

        <Drawer
          title="Business Unit"
          placement="right"
          closable={false}
          width={520}
          onClose={handleCancel}
          visible={visibleModal}
        ></Drawer>

        <Switch>
          {routes.map((route, i) => (
            <Route
              key={i}
              path={route.path}
              component={route.component}
              exact={route.exact}
            />
          ))}
        </Switch>
        <Footer style={{ textAlign: "center" }}>
          DOT ©{Dates.getFullYear()}
        </Footer>
      </Layout>
    </Layout>
  );
}

export default App;
