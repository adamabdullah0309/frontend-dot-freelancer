import React from "react";
import {
  Layout,
  Form,
  Input,
  Button,
  Row,
  Col,
  Select,
  Radio,
  Card,
  DatePicker,
  Collapse,
  Space,
  Spin,
  AutoComplete,
  InputNumber,
} from "antd";

const { Content } = Layout;

function Home(props) {
  return (
    <Card title={props.title}>
      <Row gutter={6}>
        <Col span={12}>
          <props.Form.Item label={props.label} name={props.name}>
            <AutoComplete
              options={props.province}
              placeholder={props.placeholderProvince}
              filterOption={(inputValue, option) =>
                option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !==
                -1
              }
              onSelect={props.select}
              onChange={props.change}
            />
          </props.Form.Item>
        </Col>
        <Col span={12}>
          <props.Form.Item label={props.labelKota} name={props.nameKota}>
            <Spin spinning={props.spinningKota}>
              <AutoComplete
                disabled={props.disabledKota}
                options={props.cityCard}
                placeholder={props.placeholderCity}
                filterOption={(inputValue, option) =>
                  option.value
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
                onSelect={props.selectKota}
              />
            </Spin>
          </props.Form.Item>
        </Col>
      </Row>
    </Card>
  );
}

export default Home;
