import React from "react";
import { Layout } from "antd";

const { Content } = Layout;

function Home() {
  return <Content className="site-layout-background content">Home</Content>;
}

export default Home;
