import React from "react";
import { Layout, Table } from "antd";

const { Content } = Layout;

function DetailTableCost(props) {
  const columns = [
    {
      title: "Service",
      dataIndex: "service",
      key: "service",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Estimated Arrived",
      dataIndex: "etd",
      key: "etd",
    },
    {
      title: "Cost",
      dataIndex: "value",
      key: "value",
    },
  ];
  return (
    <Table
      columns={columns}
      dataSource={
        props.result == undefined
          ? []
          : props.result.length > 0
          ? props.result[0].costs
          : []
      }
      loading={props.loadingTable}
    />
  );
}

export default DetailTableCost;
