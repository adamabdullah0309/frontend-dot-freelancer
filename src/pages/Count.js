import { React, useState, useEffect } from "react";
import DataProvince from "../providers/CallApi/DataProvince";
import DataCity from "../providers/CallApi/DataCity";
import DataCost from "../providers/CallApi/DataCost";
import DetailCard from "./DetailCard";
import DetailTableCost from "./DetailTableCost";
import {
  Layout,
  Form,
  Input,
  Button,
  Row,
  Col,
  Select,
  Radio,
  Card,
  DatePicker,
  Collapse,
  Space,
  Spin,
  AutoComplete,
  InputNumber,
} from "antd";

const { Content } = Layout;
const { Option } = Select;

function Home() {
  const [form] = Form.useForm();
  const [province, setProvince] = useState();
  const [cityFrom, setCityFrom] = useState();
  const [cityTo, setCityTo] = useState();
  const [provinceFromId, setProvinceFromId] = useState();
  const [cityFromId, setCityFromId] = useState();
  const [cityToId, setCityToId] = useState();
  const [courier, setCourier] = useState();
  const [provinceTo, setProvinceTo] = useState();
  const [numberGram, setNumberGram] = useState();
  const [result, setResult] = useState();
  const [provinceToId, setProvinceToId] = useState();
  const [visibleCityFrom, setVisibleCityFrom] = useState(true);
  const [visibleCityTo, setVisibleCityTo] = useState(true);
  const [loadingCityFromSpinning, setLoadingCityFromSpinning] = useState(false);
  const [loadingCityToSpinning, setLoadingCityToSpinning] = useState(false);
  const [loadingTable, setLoadingTable] = useState(false);

  const onGenderChange = (value) => {
    switch (value) {
      case "male":
        form.setFieldsValue({
          note: "Hi, man!",
        });
        return;

      case "female":
        form.setFieldsValue({
          note: "Hi, lady!",
        });
        return;

      case "other":
        form.setFieldsValue({
          note: "Hi there!",
        });
    }
  };

  const onFinish = (values) => {
    console.log(values);
    form
      .validateFields()
      .then((header) => {
        // values_header = { ...header };
        console.log(header, "headerr");
      })
      .catch((info) => {
        console.log(info, "info formDetail");
      });
  };

  const onReset = () => {
    form.resetFields();
  };

  const onFill = () => {
    form.setFieldsValue({
      note: "Hello world!",
      gender: "male",
    });
  };

  const getProvince = async (e) => {
    await DataProvince.getProvince({}).then((resp) => {
      console.log(resp, "resp");
      if (resp.data.rajaongkir.status.code == 200) {
        resp.data.rajaongkir.results.map((res) => {
          res.value = res.province;
          res.text = res.province;
        });
        setProvince(resp.data.rajaongkir.results);
      }
    });
  };

  const getCity = async (e, opt) => {
    console.log(e, "milih");
    if (opt == "from") {
      setLoadingCityFromSpinning(true);
    } else if (opt == "to") {
      setLoadingCityToSpinning(true);
    }
    await DataCity.getCity({ provinceId: e }).then((resp) => {
      console.log(resp, "resp");
      if (resp.data.rajaongkir.status.code == 200) {
        resp.data.rajaongkir.results.map((res) => {
          res.value = res.city_name;
        });
        if (opt == "from") {
          setCityFrom(resp.data.rajaongkir.results);
          setLoadingCityFromSpinning(false);
        } else if (opt == "to") {
          setCityTo(resp.data.rajaongkir.results);
          setLoadingCityToSpinning(false);
        }
      }
    });
  };

  useEffect(() => {
    getProvince();
  }, []);

  const selectProvinceFrom = (proId, opt) => {
    setProvinceFromId(opt.province_id);
    console.log(opt, "optis");
    getCity(opt.province_id, "from");
    setVisibleCityFrom(false);
  };

  const changProvinceFrom = (proName) => {
    province.map((pro) => {
      if (pro.value == proName) {
        setVisibleCityFrom(false);
      } else {
        setResult(null);

        form.resetFields(["cityIdFrom"]);
        setVisibleCityFrom(true);
      }
    });
  };

  const selectCityFrom = (proId, opt) => {
    setCityFromId(opt.city_id);
  };

  const selectCityTo = (proId, opt) => {
    setCityToId(opt.city_id);
  };

  const selectProvinceTo = (proId, opt) => {
    setProvinceTo(opt.province_id);
    console.log(opt, "optis");
    getCity(opt.province_id, "to");
    setVisibleCityTo(false);
  };

  const courierData = [{ value: "JNE" }, { value: "POS" }, { value: "TIKI" }];

  const changProvinceTo = (proName) => {
    province.map((pro) => {
      if (pro.value == proName) {
        setVisibleCityTo(false);
      } else {
        form.resetFields(["cityIdTo"]);
        setResult(null);
        setVisibleCityTo(true);
      }
    });
  };

  const selectCourier = (id, opt) => {
    setCourier(opt.value.toLowerCase());
  };

  const changeGram = (number) => {
    setNumberGram(number);
  };

  useEffect(() => {
    if (
      provinceFromId != null &&
      cityFromId != null &&
      cityToId != null &&
      courier != null &&
      provinceTo != null &&
      numberGram != null
    ) {
      setLoadingTable(true);
      DataCost.getCost({
        origin: cityFromId,
        destination: cityToId,
        weight: numberGram,
        courier: courier,
      }).then((resp) => {
        console.log(resp, "resp");
        if (resp.data.rajaongkir.status.code == 200) {
          var a = 1;
          resp.data.rajaongkir.results[0].costs.map((resp) => {
            resp.key = a;
            resp.value = resp.cost[0].value;
            resp.etd = resp.cost[0].etd;
            a++;
          });
          setResult(resp.data.rajaongkir.results);
          setLoadingTable(false);
        }
      });
    }
  }, [provinceFromId, cityFromId, cityToId, courier, numberGram, provinceTo]);

  console.log(result, "result");
  return (
    <Content className="site-layout-background content">
      <Form
        form={form}
        name="control-hooks"
        onFinish={onFinish}
        layout="vertical"
      >
        <Row gutter={8}>
          <Col span={12}>
            <DetailCard
              title="Origin"
              select={selectProvinceFrom}
              change={changProvinceFrom}
              province={province}
              label="Province From"
              name="provinceIdFrom"
              Form={Form}
              labelKota="City From"
              nameKota="cityIdFrom"
              spinningKota={loadingCityFromSpinning}
              onSelectKota={selectCityFrom}
              disabledKota={visibleCityFrom}
              cityCard={cityFrom}
              selectKota={selectCityFrom}
              placeholderProvince="Search Province From"
              placeholderCity="Search City From"
            />
          </Col>
          <Col span={12}>
            <Card title="Destination">
              <Row gutter={6}>
                <Col span={12}>
                  <Form.Item
                    label="Province To"
                    name="provinceIdTo"
                    rules={[
                      {
                        required: false,
                        message: "Province To is required",
                      },
                    ]}
                  >
                    <AutoComplete
                      options={province}
                      placeholder="Search Province To"
                      filterOption={(inputValue, option) =>
                        option.value
                          .toUpperCase()
                          .indexOf(inputValue.toUpperCase()) !== -1
                      }
                      onSelect={selectProvinceTo}
                      onChange={changProvinceTo}
                    />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label="City To"
                    name="cityIdTo"
                    rules={[
                      {
                        required: false,
                        message: "City To is required",
                      },
                    ]}
                  >
                    <Spin spinning={loadingCityToSpinning}>
                      <AutoComplete
                        disabled={visibleCityTo}
                        options={cityTo}
                        placeholder="Search City To"
                        filterOption={(inputValue, option) =>
                          option.value
                            .toUpperCase()
                            .indexOf(inputValue.toUpperCase()) !== -1
                        }
                        onSelect={selectCityTo}
                      />
                    </Spin>
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col span={12} offset={6}>
            <Card title="Cost Result">
              <Row gutter={6}>
                <Col span={12}>
                  <Form.Item
                    label="Courier"
                    name="courier"
                    rules={[
                      {
                        required: false,
                        message: "Courier is required",
                      },
                    ]}
                  >
                    <AutoComplete
                      options={courierData}
                      placeholder="Courier"
                      filterOption={(inputValue, option) =>
                        option.value
                          .toUpperCase()
                          .indexOf(inputValue.toUpperCase()) !== -1
                      }
                      onSelect={selectCourier}
                    />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label="Gram"
                    name="gram"
                    rules={[
                      {
                        required: false,
                        message: "Gram is required",
                      },
                    ]}
                  >
                    <InputNumber
                      style={{ width: "100%" }}
                      onChange={changeGram}
                    ></InputNumber>
                  </Form.Item>
                </Col>
                <Col span={24}>
                  {result == null ? (
                    <Spin spinning={loadingTable}>
                      <Row>
                        <Col span={12} offset={10}>
                          No Data
                        </Col>
                      </Row>
                    </Spin>
                  ) : result[0].costs.length > 0 ? (
                    <DetailTableCost
                      result={result}
                      loadingTable={loadingTable}
                    />
                  ) : (
                    <Spin spinning={loadingTable}>
                      <Row>
                        <Col span={12} offset={10}>
                          No Data
                        </Col>
                      </Row>
                    </Spin>
                  )}
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Form>
    </Content>
  );
}

export default Home;
