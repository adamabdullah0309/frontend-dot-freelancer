import React from "react";
import { Layout, Dropdown, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  SettingOutlined,
  UserOutlined,
  LockOutlined,
} from "@ant-design/icons";

import { api as helperApi } from "../helpers/api";

const { Header } = Layout;

function Navbar(props) {
  return (
    <Header
      className="site-layout-background menu-header"
      style={{ padding: 0 }}
    >
      {React.createElement(
        props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
        {
          className: "trigger",
          onClick: props.changeCollapsed,
        }
      )}
    </Header>
  );
}

export default Navbar;
