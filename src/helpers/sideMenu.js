import {
  ScheduleOutlined,
  ContainerOutlined,
  ControlOutlined,
  SelectOutlined,
  ToTopOutlined,
  CalculatorOutlined,
  CopyOutlined,
} from "@ant-design/icons";
const sideMenu = [
  // {
  //   path: "#",
  //   title: "Users",
  //   child: [
  //     {
  //       path: "/users",
  //       title: "List User",
  //     },
  //     {
  //       path: "/users/access",
  //       title: "Manage Access",
  //     },
  //   ],
  // },
  // {
  //   path: "#",
  //   title: "Order",
  //   child: [
  //     {
  //       path: "/order/my-order",
  //       title: "My Order",
  //     },
  //     {
  //       path: "/order/incoming-order",
  //       title: "Incoming Order",
  //     },
  //   ],
  // },
  // {
  //   path: "#",
  //   title: "Report",
  //   child: [
  //     {
  //       path: "/report-inbound",
  //       title: "Report Inbound",
  //     },
  //     {
  //       path: "/report-outbound",
  //       title: "Report OutBound",
  //     },
  //   ],
  // },
  // {
  //   path: "#",
  //   title: "Setting",
  //   child: [
  //     {
  //       path: "/settings/area",
  //       title: "Area",
  //     },
  //     {
  //       path: "/settings/cost",
  //       title: "Cost",
  //     },
  //     {
  //       path: "/settings/route",
  //       title: "Route",
  //     },
  //   ],
  // },

  // {
  //   path: "#",
  //   title: "Tracking",
  //   child: [
  //     {
  //       path: "/tracking/setting",
  //       title: "Setting",
  //     },
  //     {
  //       path: "/tracking/view",
  //       title: "View",
  //     },
  //     {
  //       path: "/tracking/paper",
  //       title: "Paper Tracking",
  //     },
  //   ],
  // },

  // {
  //   path: "#",
  //   title: "Claim",
  //   child: [
  //     {
  //       path: "/return-penerima",
  //       title: "Return Penerima",
  //     },
  //     {
  //       path: "/return-pengirim",
  //       title: "Return Pengirim",
  //     },
  //   ],
  // },
  {
    path: "#",
    title: "Home",
    path: "/",
    icon: <ContainerOutlined />,
  },
  {
    path: "#",
    title: "Count",
    path: "/count",
    icon: <ControlOutlined />,
  },
  // {
  //   path: "#",
  //   title: "Master Data",
  //   path: "/master-data",
  //   icon: <ControlOutlined />,
  // },
  // {
  //   path: "#",
  //   title: "Inbound",
  //   path: "/inbound",
  //   icon: <SelectOutlined />,
  // },
  // {
  //   path: "#",
  //   title: "Outbound",
  //   path: "/outbound",
  //   icon: <ToTopOutlined />,
  // },
  // {
  //   path: "#",
  //   title: "Finance",
  //   path: "/finance",
  //   icon: <CalculatorOutlined />,
  // },
  // {
  //     path: "#",
  //     title: "Contact",
  //     path: "/contact",
  //     icon: <CopyOutlined />,
  // },
  // {
  //   path: "#",
  //   title: "Contact",
  //   child: [
  //     {
  //       path: "/list-contact",
  //       title: "List Contact",
  //     },
  //     {
  //       path: "/list-contact-person",
  //       title: "List Contact Person",
  //     },
  //   ],
  // },
];

export default sideMenu;
